import {
  eventRepeatIntervalsData,
  eventTypesData,
  memberTypesData,
} from "@data";
import {
  Form,
  Input,
  Button,
  Select,
  Space,
  DatePicker,
  TimePicker,
  Checkbox,
} from "antd";

export const SessionForm = () => {
  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const onChangeStartDate = (date, dateString) => {};

  return (
    <Form
      name="basic"
      labelCol={{ span: 4 }}
      wrapperCol={{ span: 12 }}
      initialValues={{}}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="نوع رویداد"
        name="eventType"
        rules={[{ required: true, message: "فیلد اجباری" }]}
      >
        <Select
          showSearch
          className="w-100"
          placeholder="جستجو کنید"
          optionFilterProp="children"
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
          filterSort={(optionA, optionB) =>
            optionA.children
              .toLowerCase()
              .localeCompare(optionB.children.toLowerCase())
          }
        >
          {eventTypesData.map(({ name, id }) => (
            <Select.Option Option value={id}>
              {name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item label="سرفصل جلسه" name="subject">
        <Input placeholder="سرفصل مورد بحث جلسه را وارد کنید" />
      </Form.Item>
      <Form.Item label="تاریخ" name="startDate">
        <DatePicker className="w-100" onChange={onChangeStartDate} />
      </Form.Item>
      <Form.Item label="ساعت شروع" name="startTime">
        <TimePicker className="w-100" format={"HH:mm"} minuteStep={15} />
      </Form.Item>
      <Form.Item label="تکرار" name="repeat">
        <Select className="w-100" defaultValue="weekly">
          {eventRepeatIntervalsData.map(({ name, id }) => (
            <Select.Option Option value={id}>
              {name}
            </Select.Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item label="نیازمند یادآوری" name="repeat">
        <Checkbox style={{ width: "1rem", height: "1rem" }} />
      </Form.Item>
      <Form.Item label="مجاز برای" name="allowedFor">
        <Select className="w-100" defaultValue="all">
          {[{ name: "همه", id: "all" }, ...memberTypesData].map(
            ({ name, singularName, id }) => (
              <Select.Option Option value={id}>
                {name}
              </Select.Option>
            )
          )}
        </Select>
      </Form.Item>
      <Form.Item wrapperCol={{ offset: 4, span: 12 }}>
        <Button className="w-100" type="primary" htmlType="submit">
          تایید
        </Button>
      </Form.Item>
    </Form>
  );
};
