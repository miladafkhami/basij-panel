import React from "react";
import { Div, Link, Text } from "@kits";
import { Layout, Menu } from "antd";
import {
  GroupOutlined,
  FileAddOutlined,
  UnorderedListOutlined,
  TeamOutlined,
  UserAddOutlined,
  MessageOutlined,
  VideoCameraOutlined,
  ScheduleOutlined,
  WarningOutlined,
  UserSwitchOutlined,
  HistoryOutlined,
  FileDoneOutlined,
} from "@ant-design/icons";

const menuItems = [
  {
    key: 1,
    label: "رویداد ها",
    Icon: GroupOutlined,
    subMenu: [
      {
        key: 11,
        link: "/event/assign",
        label: " تخصیص رویداد",
        Icon: FileAddOutlined,
      },
      {
        key: 12,
        link: "/event/list",
        label: "لیست رویدادها",
        Icon: UnorderedListOutlined,
      },
    ],
  },
  {
    key: 2,
    label: "جلسه ها",
    Icon: TeamOutlined,
    subMenu: [
      {
        key: 21,
        link: "/session/create",
        label: "ایجاد جلسه",
        Icon: FileAddOutlined,
      },
      {
        key: 22,
        link: "/session/list",
        label: "لیست جلسات",
        Icon: UnorderedListOutlined,
      },
    ],
  },
  {
    key: 3,
    label: "اعضاء",
    Icon: TeamOutlined,
    subMenu: [
      {
        key: 31,
        link: "/session/create",
        label: "ایجاد عضو",
        Icon: UserAddOutlined,
      },
      {
        key: 32,
        link: "/session/list",
        label: "لیست اعضاء",
        Icon: UnorderedListOutlined,
      },
    ],
  },
  {
    key: 4,
    label: "گزارش گیری",
    Icon: FileDoneOutlined,
    subMenu: [
      {
        key: 41,
        link: "/report/events",
        label: "گزارش گیری رویداد ها",
        Icon: GroupOutlined,
      },
      {
        key: 41,
        link: "/report/members",
        label: "گزارش گیری اعضاء",
        Icon: UserSwitchOutlined,
      },
    ],
  },
  { key: 5, link: "/messages", label: "پیام ها", Icon: MessageOutlined },
  { key: 6, link: "/guide", label: "آموزش", Icon: VideoCameraOutlined },
  { key: 7, link: "/calendar", label: "تقویم", Icon: ScheduleOutlined },
  {
    key: 8,
    link: "/review",
    label: "پیشنهادات و گزارش مشکل",
    Icon: WarningOutlined,
  },
];

export function renderMenuItems(menuItems) {
  return menuItems.map(({ key, label, link, Icon, subMenu }) =>
    subMenu?.length ? (
      <Menu.SubMenu
        icon={<Icon style={{ fontSize: 20 }} />}
        title={<Text size="lg">{label}</Text>}
      >
        {renderMenuItems(subMenu)}
      </Menu.SubMenu>
    ) : (
      <Menu.Item key={key} icon={<Icon style={{ fontSize: 20 }} />}>
        {link ? (
          <Link href={link}>
            <Text size="lg">{label}</Text>
          </Link>
        ) : (
          <Text size="lg">{label}</Text>
        )}
      </Menu.Item>
    )
  );
}

export function Sidebar(props = {}) {
  const { children, hideHeader, hideFooter } = props;

  return (
    <Layout.Sider
      width="var(--layout-side-bar-width)"
      style={{ paddingTop: "var(--spacing-2)" }}
    >
      <Menu mode="inline">{renderMenuItems(menuItems)}</Menu>
    </Layout.Sider>
  );
}
