import React, { Fragment } from "react";
import { Div, Link, Text } from "@kits";
import { Layout, Menu } from "antd";
import {
  GroupOutlined,
  FileAddOutlined,
  UnorderedListOutlined,
  TeamOutlined,
  UserAddOutlined,
  MessageOutlined,
  VideoCameraOutlined,
  ScheduleOutlined,
  WarningOutlined,
} from "@ant-design/icons";

const menuItems = [
  {
    key: 1,
    label: "رویداد ها",
    Icon: GroupOutlined,
    subMenu: [
      {
        key: 11,
        link: "/event/assign",
        label: " تخصیص رویداد",
        Icon: FileAddOutlined,
      },
      {
        key: 12,
        link: "/event/list",
        label: "لیست رویدادها",
        Icon: UnorderedListOutlined,
      },
    ],
  },
  {
    key: 2,
    label: "جلسه ها",
    Icon: TeamOutlined,
    subMenu: [
      {
        key: 21,
        link: "/session/create",
        label: "ایجاد جلسه",
        Icon: FileAddOutlined,
      },
      {
        key: 22,
        link: "/session/list",
        label: "لیست جلسات",
        Icon: UnorderedListOutlined,
      },
    ],
  },
  {
    key: 3,
    label: "اعضاء",
    Icon: TeamOutlined,
    subMenu: [
      {
        key: 21,
        link: "/member/create",
        label: "ایجاد عضو",
        Icon: UserAddOutlined,
      },
      {
        key: 22,
        link: "/member/list",
        label: "لیست اعضاء",
        Icon: UnorderedListOutlined,
      },
    ],
  },
  {
    key: 4,
    link: "/messages",
    label: "پیام ها",
    Icon: MessageOutlined,
  },
  {
    key: 5,
    link: "/guide",
    label: "آموزش",
    Icon: VideoCameraOutlined,
  },
  {
    key: 6,
    link: "/calendar",
    label: "تقویم",
    Icon: ScheduleOutlined,
  },
  {
    key: 7,
    link: "/criticism",
    label: "پیشنهادات و گزارش مشکل",
    Icon: WarningOutlined,
  },
];

export function MenuItems(props) {
  const { list = menuItems } = props;

  return list.map(({ key, label, link, Icon, subMenu }) =>
    subMenu?.length ? (
      <Fragment key={key}>
        <Menu.SubMenu icon={<Icon />} title={<Text size="lg">{label}</Text>}>
          {/* {renderMenuItems(subMenu)} */}
          <MenuItems list={subMenu} />
        </Menu.SubMenu>
      </Fragment>
    ) : (
      <Fragment key={key}>
        <Menu.Item key={key} icon={<Icon />}>
          {link ? (
            <Link href={link}>
              <Text size="lg">{label}</Text>
            </Link>
          ) : (
            <Text size="lg">{label}</Text>
          )}
        </Menu.Item>
      </Fragment>
    )
  );
}
