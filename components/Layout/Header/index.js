import React from "react";
import { Div } from "@kits";
import { Layout } from "antd";
import { HeaderLogo } from "./Logo";
import { HeaderProfile } from "./Profile";

export function Header(props) {
  const {} = props || {};

  return (
    <Div as={Layout.Header} height="var(--layout-header-height)" px="4">
      <Div flex={["center", "between"]}>
        <HeaderLogo />
        <HeaderProfile />
      </Div>
    </Div>
  );
}
