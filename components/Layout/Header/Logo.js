import React from "react";
import { Div, Image, Link, Text } from "@kits";

export function HeaderLogo(props) {
  const {} = props || {};

  return (
    <Div height="var(--layout-header-height)" flex={["center"]}>
      <Div
        flex={["center"]}
        width="var(--layout-header-height)"
        height="100%"
        p="3"
      >
        <Link href="/" position="relative" matchParent>
          <Image
            src="/images/logo-lg.png"
            placeholder="blur"
            blurDataURL="/images/logo-lg.png"
            layout="fill"
          />
        </Link>
      </Div>
      <Div>
        <Text size="h-sm-b">app.title</Text>
      </Div>
    </Div>
  );
}
