import React from "react";
import { Div, Text } from "@kits";
import { Avatar, Divider } from "antd";
import { UserOutlined } from "@ant-design/icons";

export function HeaderProfile(props) {
  const {} = props || {};

  return (
    <Div
      height="3rem"
      curve="xsm"
      border="border-dark"
      flex={["center", "start"]}
      p="1"
    >
      <Div ml="2">
        <Div
          bg="bg-secondary"
          height="2.25rem"
          width="2.25rem"
          curve="xsm"
          flex={["center", "center"]}
        >
          <UserOutlined
            style={{ color: "var(--color-text-secondary)", fontSize: "20px" }}
          />
        </Div>
      </Div>
      <Div>
        <Text size="md-b">پایگاه شهدای 15 خرداد</Text>
      </Div>
      <Divider
        type="vertical"
        style={{ height: "100%", borderColor: "var(--color-border-dark)" }}
      />
      <Div>
        <Text size="sm">حوزه 5 امام حسین</Text>
      </Div>
    </Div>
  );
}
