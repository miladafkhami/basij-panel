import React from "react";
import { Div } from "@kits";
import { APP_WRAPPER_ELEMENT_ID } from "@config";
// import { Header } from "./Header";
import dynamic from "next/dynamic";
import { Layout, Menu } from "antd";
import { Sidebar } from "./Sidebar";
import { Header } from "./Header";

export function MyLayout(props = {}) {
  const { children, hideHeader, hideFooter } = props;

  return (
    <Div position="relative" id={APP_WRAPPER_ELEMENT_ID} height="100vh">
      <Layout style={{ height: "100%" }}>
        <Header />
        <Layout>
          <Sidebar />
          <Layout>
            <Layout.Content>
              <Div p="0 3 2 3" overflowX="auto" height="100%">
                {children}
              </Div>
            </Layout.Content>
          </Layout>
        </Layout>
      </Layout>
    </Div>
  );
}
