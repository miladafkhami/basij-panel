import { endpoints } from "@constants";
import { useSWRImmutable } from "@hooks";

export function useDashboard() {
  const swr = useSWRImmutable(endpoints.dashboard);

  return swr;
}
