const { PHASE_DEVELOPMENT_SERVER } = require("next/constants");

/** @type {import("next").NextConfig} */
module.exports = {
  env: {
    REACT_APP_BASE_URL: "https://app.basij.com",
    REACT_APP_API_BASE_URL: "/api/proxy",
    REACT_APP_EXTERNAL_API_BASE_URL: "/api",
  },
  i18n: {
    locales: ["fa"],
    defaultLocale: "fa",
  },
  swcMinify: true,
  experimental: {
    craCompat: true,
  },
  compiler: {
    styledComponents: true,
  },
  // images: {
  //   domains: [],
  // },
};
