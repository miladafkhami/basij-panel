import React from "react";
import "../index.css";
import { MyLayout } from "@components/Layout";
import { useRouter } from "next/router";
import { ThemeProvider } from "next-themes";
import { defaultTheme, GlobalStyles } from "@stylesheets";
import { THEMES } from "@constants";
import { swrFetcher, __pick } from "@utils";
import { DefaultSeo } from "@components/SEO";
import { enhanceStringPrototype, LanguageHelper } from "@helpers";
import { useEffect, useTranslation } from "@hooks";
import { SWRConfig } from "swr";
// ant design
import ConfigProvider from "antd/lib/config-provider";
import fa_locales from "antd/lib/locale/fa_IR";
import "antd/dist/antd.css";

function MyApp({ Component, pageProps, ...rest }) {
  const router = useRouter();
  const t = useTranslation().t;

  enhanceStringPrototype();

  useEffect(() => {
    LanguageHelper.initialLanguageSetup();

    // router.events.on("routeChangeStart", nProgress.start);
    // router.events.on("routeChangeComplete", nProgress.done);
    // router.events.on("routeChangeError", nProgress.done);
  }, []);

  return (
    <SWRConfig value={{ fetcher: swrFetcher }}>
      <ConfigProvider direction="rtl" locale={fa_locales}>
        <GlobalStyles />
        <ThemeProvider
          themes={Object.values(THEMES)}
          defaultTheme={defaultTheme}
          enableSystem={false}
        >
          <DefaultSeo
            defaultTitle={t("seo.title.default")}
            description={t("seo.description.default")}
          />
          <MyLayout {...__pick(Component, ["hideHeader", "hideFooter"])}>
            <Component {...pageProps} />
          </MyLayout>
        </ThemeProvider>
      </ConfigProvider>
    </SWRConfig>
  );
}

export default MyApp;
