import React from "react";
import { Div } from "@kits";
import { Head } from "@components/SEO";

export default function ReviewPage(props) {
  return (
    <>
      <Head canonical="/" />
      <Div width="100%" py="3">
        ReviewPage
      </Div>
    </>
  );
}
