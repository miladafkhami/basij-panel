import React from "react";
import { Div } from "@kits";
import { Head } from "@components/SEO";

export default function GuidePage(props) {
  return (
    <>
      <Head canonical="/" />
      <Div width="100%" py="3">
        GuidePage
      </Div>
    </>
  );
}
