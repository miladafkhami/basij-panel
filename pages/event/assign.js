import React from "react";
import { Div } from "@kits";
import { Head } from "@components/SEO";
import { Divider, PageHeader } from "antd";
import { AssignEventForm } from "@components/Event";

export default function AssignEventPage(props) {
  return (
    <>
      <Head page="event.assign" />
      <Div width="100%">
        <PageHeader title="تخصیص رویداد" />
        <Divider />
        <AssignEventForm />
      </Div>
    </>
  );
}
