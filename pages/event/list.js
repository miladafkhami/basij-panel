import React from "react";
import { Div } from "@kits";
import { Head } from "@components/SEO";

export default function EventListPage(props) {
  return (
    <>
      <Head title="" />
      <Div width="100%">Event List page</Div>
    </>
  );
}
