import React from "react";
import { Div } from "@kits";
import { Head } from "@components/SEO";
import { SessionForm } from "@components/Session";
import { Divider, PageHeader } from "antd";

export default function CreateSessionPage(props) {
  return (
    <>
      <Head page="session.create" />
      <Div width="100%">
        <PageHeader title="ایجاد جلسه" />
        <Divider />
        <SessionForm />
      </Div>
    </>
  );
}
