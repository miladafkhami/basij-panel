export const eventTypesData = [
  { name: "طرح صالحین", id: "tarhe-salehin" },
  { name: "ورزش", id: "aports" },
  { name: "گشت شبانه", id: "night-patrol" },
  { name: "قرآن", id: "quran" },
];

export const eventRepeatIntervalsData = [
  { name: "روزانه", id: "daily" },
  { name: "یک روز در میان", id: "every-second-day" },
  { name: "هفتگی", id: "weekly" },
  { name: "ماهانه", id: "monthly" },
];

export const memberTypesData = [
  { name: "اعضای شورا", singularName: "عضو شوراء", id: "council" },
  { name: "اعضای فعال", singularName: "عضو فعال", id: "active" },
  { name: "اعضای عادی", singularName: "عضو عادی", id: "common" },
  { name: "اعضای کادری", singularName: "عضو کادری", id: "special" },
];
